# CpHMD patches on different released Amber versions

## Prerequisites

A. A working Amber with version >= 18.0

B. All other patches applied

## How to apply a patch

### A. Download a specific patch file for the installed Amber version

1. `amber18_gb_cphmd.patch` for fully patched Amber 18
2. `amber20_gb_cphmd.patch` for fully patched Amber 20

### B. Copy the patch file to `${AMBERSOURCE}/src/pmemd` where `AMBERSOURCE` is the home directory for the Amber version to be patched.

### C. Apply the patch. Using the same example above, under `${AMBERSOURCE}/src/pmemd`, type

    patch -s -p0 < amberxx_gb_cphmd.patch

### D. Build and install the patched version of pmemd following the instructions for a normal Amber installation

## Please cite the following papers

1. Harris RC and Shen J*, GPU-accelerated implementation of continuous constant pH molecular dynamics in Amber: pKa predictions with single pH simulations. _J. Chem. Inf. Model._ 59: 4821-4832, 2019.

2. Huang YD, Harris RC, Shen J*, Generalized Born based continuous constant pH molecular dynamics in Amber: implementation, benchmarking, and analysis. _J. Chem. Inf. Model._ 58:1372-1383, 2018.
